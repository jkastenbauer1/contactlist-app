package com.example.contactlistapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ScrollView;

public class ContactSettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_settings);
		initListButton();
        initMapButton();
        initSettingsButton();
        initSettings();
        initSortByClick();
        initSortOrderClick();
        initBackgroundClick();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.contact_settings, menu);
		return true;
	}
	
	private void initSettings() {
		String sortBy = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortfield","contactname"); // 1 
		String sortOrder = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("sortorder","ASC");
		String backgroundColor = getSharedPreferences("MyContactListPreferences", Context.MODE_PRIVATE). getString("backgroundcolor","Pale");
		
		RadioButton rbName = (RadioButton) findViewById( R.id.radioName); // 2
		RadioButton rbCity = (RadioButton) findViewById( R.id.radioCity);
		RadioButton rbBirthDay = (RadioButton) findViewById( R.id.radioBirthday);
		if (sortBy.equalsIgnoreCase("contactname")) { // 3 
			rbName.setChecked( true); 
			} 
		else if (sortBy.equalsIgnoreCase("city")) { 
			rbCity.setChecked( true); 
			} 
		else { 
			rbBirthDay.setChecked( true); 
			}
		
		RadioButton rbAscending = (RadioButton) findViewById(R.id.radioAscending);
		RadioButton rbDescending = (RadioButton) findViewById(R.id.radioDescending);
		if (sortOrder.equalsIgnoreCase("ASC")) {
			rbAscending.setChecked(true);
		}
		else {
			rbDescending.setChecked(true);
		}
		
		RadioButton rbPale = (RadioButton) findViewById(R.id.radioBG1);
		RadioButton rbDark = (RadioButton) findViewById(R.id.radioBG2);
		ScrollView scrollviewobject = (ScrollView) findViewById(R.id.scrollView1);
		if (backgroundColor.equalsIgnoreCase("Pale")) {
			rbPale.setChecked(true);
			scrollviewobject.setBackgroundResource(R.color.contactsetting_background1);
		}
		else {
			rbDark.setChecked(true);
			scrollviewobject.setBackgroundResource(R.color.contactsetting_background2);
		}
	}
	
	private void initSortByClick() {
		RadioGroup rgSortBy = (RadioGroup) findViewById( R.id.radioGroup3);
		rgSortBy.setOnCheckedChangeListener( new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged( RadioGroup arg0, int arg1) {
				RadioButton rbName = (RadioButton) findViewById( R.id.radioName);
				RadioButton rbCity = (RadioButton) findViewById( R.id.radioCity);
				if (rbName.isChecked()) {
					getSharedPreferences("MyContactListPreferences", MODE_PRIVATE). edit() .putString("sortfield", "contactname"). commit();
					}
				else if (rbCity.isChecked()) {
					getSharedPreferences("MyContactListPreferences", MODE_PRIVATE). edit() .putString("sortfield", "city"). commit();
					}
				else { 
					getSharedPreferences("MyContactListPreferences", MODE_PRIVATE). edit() .putString("sortfield", "birthday"). commit(); 
					} 
				} }); 
		}
	
	private void initSortOrderClick() {
		RadioGroup rgSortOrder = (RadioGroup) findViewById(R.id.radioGroup4);
		rgSortOrder.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(RadioGroup arg0, int arg1){
				RadioButton rbAscending = (RadioButton) findViewById(R.id.radioAscending);
				if(rbAscending.isChecked()){
					getSharedPreferences("MyContactListPreferences", MODE_PRIVATE).edit().putString("sortorder", "ASC").commit();
				}
				else {
					getSharedPreferences("MyContactListPreferences", MODE_PRIVATE).edit().putString("sortorder", "DESC").commit();
				}
			}
		});
	}
	
	private void initBackgroundClick() {
		RadioGroup rgBackgroundColor = (RadioGroup) findViewById(R.id.radioGroup5);
		rgBackgroundColor.setOnCheckedChangeListener(new OnCheckedChangeListener(){
			@Override
			public void onCheckedChanged(RadioGroup arg0, int arg1){
				RadioButton rbPale = (RadioButton) findViewById(R.id.radioBG1);
				ScrollView scrollviewobject = (ScrollView) findViewById(R.id.scrollView1);
				
		        if(rbPale.isChecked()){
					getSharedPreferences("MyContactListPreferences", MODE_PRIVATE).edit().putString("backgroundcolor", "Pale").commit();
					scrollviewobject.setBackgroundResource(R.color.contactsetting_background1);
				}
				else {
					getSharedPreferences("MyContactListPreferences", MODE_PRIVATE).edit().putString("backgroundcolor", "Dark").commit();
					scrollviewobject.setBackgroundResource(R.color.contactsetting_background2);
					
				}
			}
		});
	}

	
	
	private void initListButton() {
        ImageButton list = (ImageButton) findViewById(R.id.imageButtonList);
        list.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
    			Intent intent = new Intent(ContactSettingsActivity.this, ContactListActivity.class);
    			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    			startActivity(intent);
            }
        });
	}
	
	private void initMapButton() {
        ImageButton list = (ImageButton) findViewById(R.id.imageButtonMap);
        list.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
    			Intent intent = new Intent(ContactSettingsActivity.this, ContactMapActivity.class);
    			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    			startActivity(intent);
            }
        });
	}
	private void initSettingsButton() {
        ImageButton list = (ImageButton) findViewById(R.id.imageButtonSettings);
        list.setEnabled(false);
//        list.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//    			Intent intent = new Intent(ContactSettingsActivity.this, ContactSettingsActivity.class);
//    			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//    			startActivity(intent);
//            }
//        });
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
