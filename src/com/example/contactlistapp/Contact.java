package com.example.contactlistapp;

import android.text.format.Time;

public class Contact { 
	private int contactID; 
	private String contactName; 
	private ContactAddress contactAddress;
	private String phoneNumber; 
	private String cellNumber; 
	private String eMail;
	//private String bff;
	private Time birthday; 
	public Contact() { 
		setContactID(-1);
		contactName = "";
		phoneNumber = "";
		cellNumber = "";
		eMail = "";
		Time t = new Time(); 
		t.setToNow(); 
		setBirthday(t); 
		contactAddress = new ContactAddress();
		}
	public int getContactID() {
		return contactID;
	}
	public void setContactID(int contactID) {
		this.contactID = contactID;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getEMail() {
		return eMail;
	}
	public void setEMail(String eMail) {
		this.eMail = eMail;
	}
	public Time getBirthday() {
		return birthday;
	}
	public void setBirthday(Time birthday) {
		this.birthday = birthday;
	}
	public ContactAddress getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(ContactAddress contactAddress) {
		this.contactAddress = contactAddress;
	}
	/*
	public String getBff() {
		return bff;
	}
	public void setBff(String bff) {
		this.bff = bff;
	}*/ 
	
}

