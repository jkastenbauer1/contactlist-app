package com.example.contactlistapp;

import java.util.ArrayList;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class ContactAdapter extends ArrayAdapter < Contact > { // 2 
	private ArrayList < Contact > items; 
	private Context adapterContext; // 3 
	public ContactAdapter( Context context, ArrayList < Contact > items) { 
		super( context, R.layout.list_item, items); 
		this.adapterContext = context; 
		this.items = items; 
		} // 4 
	
	@Override 
	public View getView( int position, View convertView, ViewGroup parent) { 
		View v = convertView; 
		try { 
			Contact contact = items.get( position); // 5 
			if (v == null) { 
				LayoutInflater vi = (LayoutInflater) adapterContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE); 
				v = vi.inflate( R.layout.list_item, null); 
				} // 6 
			TextView contactName = (TextView) v.findViewById( R.id.textContactName); 
			TextView contactPhoneNumber = (TextView) v.findViewById( R.id.textPhoneNumber); 
			TextView contactCellNumber = (TextView) v.findViewById( R.id.textCellNumber);
			TextView contactAddress = (TextView) v.findViewById( R.id.textAddress);
			Button b = (Button) v.findViewById( R.id.buttonDeleteContact); 
			contactName.setText( contact.getContactName()); 
			if (position%2 == 0){
				contactName.setTextColor(ContextCompat.getColor(getContext(), R.color.system_red));
			}
			else{
				contactName.setTextColor(ContextCompat.getColor(getContext(), R.color.system_blue));
			}
			contactPhoneNumber.setText( contact.getPhoneNumber()); 
			contactCellNumber.setText( contact.getCellNumber());
			ContactAddress address = contact.getContactAddress();
			contactAddress.setText( address.getStreetAddress()+" "+address.getCity()+" "+address.getState()+" "+address.getZipCode());
			b.setVisibility( View.INVISIBLE); 
			} 
		catch (Exception e) { 
			e.printStackTrace(); 
			e.getCause(); 
			} 
		return v;
		}
	
	public void showDelete( final int position, final View convertView, // 1 
			final Context context, final Contact contact) { 
		View v = convertView; 
		final Button b = (Button) v.findViewById( R.id.buttonDeleteContact); // 2 
		if (b.getVisibility() == View.INVISIBLE) { 
			b.setVisibility( View.VISIBLE); 
			b.setOnClickListener( new OnClickListener() { 
				@Override public void onClick( View v) { 
					hideDelete( position, convertView, context); 
					items.remove( contact); 
					deleteOption( contact.getContactID(), context); 
					} 
				}); 
			} 
		else { 
			hideDelete( position, convertView, context); 
			} 
		}
	
	private void deleteOption( int contactToDelete, Context context) { 
		ContactDataSource db = new ContactDataSource( context); 
		db.open(); 
		db.deleteContact( contactToDelete);
		db.close(); 
		this.notifyDataSetChanged(); 
		} // 4 
	
	public void hideDelete( int position, View convertView, Context context) { 
		View v = convertView; 
		final Button b = (Button) v.findViewById( R.id.buttonDeleteContact); 
		b.setVisibility( View.INVISIBLE); 
		b.setOnClickListener( null); 
		}

			
}


